
public class Ant {
	
	
	private int id; //unique integer id that determines the order in which the ant takes its turn 
	private boolean isRed;
	private int state; //An integer between 0 and 9999 representing the current state of its brain.
	private int resting; //An integer resting that keeps track of how long the ant has to rest after its last move before any other action. 
	private Direction direction;
	private boolean hasFood;
	private Position position;

	
	public Ant(int i, boolean isRed,Position pos){
		this.id = i;
		this.isRed = isRed;
		this.position =pos;
		this.direction = Direction.EAST;
		this.hasFood = false;
	}
	
	
	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return the resting
	 */
	public int getResting() {
		return resting;
	}

	/**
	 * @param resting the resting to set
	 */
	public void setResting(int resting) {
		this.resting = resting;
	}

	/**
	 * @return the direction
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	/**
	 * @return the hasFood
	 */
	public boolean hasFood() {
		return hasFood;
	}

	/**
	 * @param hasFood the hasFood to set
	 */
	public void setHasFood(boolean hasFood) {
		this.hasFood = hasFood;
	}

	/**
	 * @return the ant's id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the ant's position
	 */
	public Position getPos() {
		return this.position;
	}
	
	public void setPosition(Position pos) {
		this.position = pos;
		
	}
	
	public boolean isRed(){
		return this.isRed;
	}
		
	public String toString(){
		String colorString = this.isRed() ? "Red" : "Black"; 
		return colorString +" Ant "+this.id +" at "+this.position.toString();
	}
}
