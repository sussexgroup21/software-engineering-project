public class AntBrain{
	Instruction[] brain;
	
	public AntBrain(Instruction[] list){
		this.brain = list;
	}
	
	public Instruction getInstruction(int state){
		return this.brain[state];
	}
	
	public String toString(){
		String out = "";
		out += "---- BRAIN START ----";
		for (int i = 0; i < this.brain.length; i++) {
			out += this.brain[i] + "\n";
		}
		out += "---- BRAIN END ----";
		
		return out;
	}
}