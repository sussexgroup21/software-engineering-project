import java.io.*;
import java.util.ArrayList;

public class AntBrainParser{
	
	public static boolean checkFile(String fileLoc){
		boolean isCorrectSoFar = true;

		// Regex for instruction syntax
		String i = "(0|1|2|3|4|5)"; // markers
		String st = "(\\d\\d?\\d?\\d?)"; // state
		String p = "(\\d+)"; // probability values for flip (any integer)
		String lr = "(Left|Right)"; // Left or Right
		String cond = "(Friend|Foe|FriendWithFood|FoeWithFood|Food|Rock|FoeMarker|Home|FoeHome|(Marker "+i+"))"; // conditions
		String sensedir = "(Here|Ahead|LeftAhead|RightAhead)"; // Sense directions
		String unmark = "Unmark "+i+" "+st;
		String mark = "Mark "+i+" "+st;
		String sense = "Sense "+sensedir+" "+st+" "+st+" "+cond;
		String pickup = "PickUp "+st+" "+st;
		String drop = "Drop "+st;
		String turn = "Turn "+lr+" "+st;
		String move = "Move "+st+" "+st;
		String flip = "Flip "+p+" "+st+" "+st;

		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileLoc)));
			System.out.println("CHECKING BRAIN "+"'"+fileLoc+"'");
			
			String rawLine ="";
			int lineNum = 1;
			while ((rawLine = reader.readLine()) != null && isCorrectSoFar){
				String line = rawLine.split(";")[0].trim();
				if( !line.matches(mark) && !line.matches(unmark) && !line.matches(sense) && !line.matches(pickup) && !line.matches(drop) && !line.matches(turn) && !line.matches(move) && !line.matches(flip)){
					isCorrectSoFar = false;
					System.out.println("[BRAIN ERROR] Bad syntax at line:"+lineNum+" ("+ line+")");
				} 
				lineNum++;
			}
			reader.close();
		}
		catch (Exception e){
			System.out.println("it's broken obviously.");
			e.printStackTrace();
		}

		return isCorrectSoFar;
	}

	public static Instruction[] parseFile(String fileLoc){
		Instruction[] parsedArray = null;
		
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileLoc)));
			ArrayList<Instruction> instructionList = new ArrayList<Instruction>();
	
			String line ="";		
			while ((line = reader.readLine()) != null){
				String[] instr = line.split(" ");
				
				InstrEnum i = InstrEnum.valueOf(instr[0]);
				
				switch (i) {
					case Mark:
						Mark m = new Mark("Mark",instr[1],instr[2]);
						instructionList.add(m);
						break;
					
					case Sense:
						Sense s;
						if(instr.length==5){
							s = new Sense("Sense",instr[1],instr[2],instr[3],instr[4]);
						} else {
							s = new Sense("Sense",instr[1],instr[2],instr[3],instr[4]+" "+instr[5]);
						}
						instructionList.add(s);
						
						break;
						
					case Unmark:
						Unmark u = new Unmark("Unmark",instr[1],instr[2]);
						instructionList.add(u);
						break;
						
					case PickUp:
						PickUp p = new PickUp("PickUp",instr[1],instr[2]);
						instructionList.add(p);
						break;
						
					case Drop:
						Drop d = new Drop("Drop",instr[1]);
						instructionList.add(d);
						break;
						
					case Turn:
						Turn t = new Turn("Turn",instr[1],instr[2]);
						instructionList.add(t);
						break;
						
					case Move:
						Move mo = new Move("Move",instr[1],instr[2]);
						instructionList.add(mo);
						break;
						
					case Flip:
						Flip f = new Flip("Flip",instr[1],instr[2],instr[3]);
						instructionList.add(f);
						break;			
				}			
			}
			parsedArray = new Instruction[instructionList.size()];
			for(int i=0; i<instructionList.size();i++){
				parsedArray[i] = instructionList.get(i);
			}
			
			reader.close();
		}catch (FileNotFoundException e){
			System.out.println("No such file exists!");
			e.printStackTrace();
		}catch (IOException e){
			System.out.println("Error reading file!");
			e.printStackTrace();
		}
		
		return parsedArray;
	}
}