import processing.core.*;
import java.util.LinkedList;
import java.util.List;

public class AntGameGUI extends PApplet{
	PFont f;
	AntWorld world;
	boolean run;
	
	boolean validWorld = false;
	
	int backgroundColor = 255;
	
	Hexagon[][] hexagons;
	Hexagon[][] bgHexagons;
	
	public AntGameGUI(String w, String rbrain, String bbrain){
		world = new AntWorld(w,rbrain,bbrain);	
	}
	
	/**
	 * Just used for testing
	 */
	public AntGameGUI(){
		world = new AntWorld("./worlds/sample.world","./brains/stupid.brain","./brains/stupid.brain");
	}

	@Override
	public void setup() {
		size(2500,2500);
		run = false;

		f = createFont("Arial",10,true);
		textFont(f,10);
		frameRate(120);
		background(backgroundColor);
//		smooth(8);
		noSmooth();
		System.out.println("Game start!");
		createHexagons(world.getCells());
	}
	
	@Override
	public void draw() {
//		for (int i = 0; i < 1000; i++) {
			world.playRound();
//		}
		drawHexagons(world.getCells());

		drawStats();
		if(GamePanel.jtfBlackScore != null){
			GamePanel.jtfBlackScore.setText("Black: "+this.world.getBlackFood());
			GamePanel.jtfRedScore.setText("Red: "+this.world.getRedFood());
			
			GamePanel.jtfBlackNumAnts.setText("Black: "+this.world.getBlackNumAnts());
			GamePanel.jtfRedNumAnts.setText("Red: "+this.world.getRedNumAnts());
		}
	}
	
	void drawStats(){
		fill(255);
		text("Round #"+(world.getRound()-1),10,10);
		
		fill(0);
		text("Round #"+world.getRound(),10,10);
	}
	
	int setCellColor(Cell c){
//		System.out.println(c.getCellType());
		String cellType = c.getCellType();
		int outputColor;
		
		if(c.hasAnt()){
			if(c.getAnt().isRed()){
				// red ant
				outputColor = color(255,0,0);
			}else{
				// black ant
				outputColor = color(0,0,0);
			}
		}else if(cellType.equals("rock")){
			outputColor = color(122,122,122);
		}else if(cellType.equals("clear") || cellType.equals("food") || cellType.equals("red hill") || cellType.equals("black hill")){
			switch(c.getFood()){ // is this even needed cos there are like a squillion foods. meh
			case 0:
				outputColor = color(230,230,230);
				break;
			case 1:
				outputColor = color(255,252,0);
				break;
			case 2:
				outputColor = color(253,234,0);
				break;
			case 3:
				outputColor = color(248,196,0);
				break;
			case 4:
				outputColor = color(88,196,156);
				break;
			case 5:
				outputColor = color(77,210,119);
				break;
			case 6:
				outputColor = color(57,235,54);
				break;
			case 7:
				outputColor = color(125,143,234);
				break;
			case 8:
				outputColor = color(119,82,197);
				break;
			case 9:
				outputColor = color(115,25,162);
				break;
			default:
//				System.out.println("food level: "+c.getFood());
				outputColor = color(0,0,0);
			}
		}else{
			// if it can't find a colour for this cell, make some ugly green colour
			outputColor = color(30,255,0);
		}
		
		return outputColor;
	}
	
	void drawHexagons(Cell[][] cells){
		List<Position> posns = positionsToUpdate(cells);
		
		for (Position position : posns) {
//			int row = position.getRow();
//			int col = position.getCol();
			
			noStroke();
			Hexagon h = getHexagonAt(position);
			Hexagon bh = getBGHexagonAt(position);
			Cell c = getCellAt(cells, position);
			
			bh.draw(backgroundColor);
			
			// borders for ant hills
			if(c.cellType().equals("red hill")){
				// draw red border
				stroke(255,0,0);
//				strokeWeight(0);
			} else if(c.cellType().equals("black hill")){
				// draw black border
				stroke(0,0,0);
//				strokeWeight(0);
			}else{
				noStroke();
			}
			
			int nextColor = setCellColor(c);
			h.draw(nextColor);
			
			// pheromones/markers!!
			h.drawMarkers(c.getMarkers(true), c.getMarkers(false));

			if(c.hasAnt()){
				if(c.getAnt().hasFood()){
					fill(255,234,0);
					noStroke();
					ellipse(h.getxPos(),h.getyPos(),4,4);
				}
				drawAntDirectionArrow(h,c);
			}
		}
	}
	
	List<Position> positionsToUpdate(Cell[][] cells){
		LinkedList<Position> posns = new LinkedList<Position>();
		for (int row = 0;row < hexagons.length;row++) {
			for (int col = 0;col < hexagons[0].length;col++) {
				Position pos = new Position(row,col);
				Cell c = getCellAt(cells, pos);
				Hexagon h = getHexagonAt(pos);
				
				int nextColor = setCellColor(c);
				
				if(c.hasAnt() || nextColor != h.getColor()){
					posns.add(pos);
					// draw adjacent cells
//					posns.addAll(world.adjacentCells(pos));
				}
			}
		}
		
		return posns;
	}
	
	void createHexagons(Cell[][] cells){		
		int numCols = cells.length;
		int numRows = cells[0].length;
		
		int sideLength = 10;
		
		int hexWidth = (int) (cos(radians(30))*sideLength) * 2;
		int hexHeight = (int) sideLength + sideLength / 2 - 2;
		
		int startX = hexWidth/2;
		int startY = hexHeight;
		
		size(startX + hexWidth * numCols, 160 + hexHeight * numRows);
		
		hexagons = new Hexagon[numRows][numCols];
		bgHexagons = new Hexagon[numRows][numCols];
		
		for (float row = 0;row < numRows;row++) {
			for (float col = 0;col < numCols;col++) {
				int xPos = (int)(startX + (col + ((row+1)%2)/2) * hexWidth);
                int yPos = (int)(startY + (row+row * hexHeight));

				hexagons[(int)row][(int)col] = new Hexagon(this, xPos, yPos, sideLength - 2);
				bgHexagons[(int)row][(int)col] = new Hexagon(this, xPos, yPos, sideLength - 1);
			}
		}
	}
	
	private Hexagon getHexagonAt(Position pos){
		int row = pos.getRow();
		int col = pos.getCol();
		
		return hexagons[row][col];
	}
	
	private Hexagon getBGHexagonAt(Position pos){
		int row = pos.getRow();
		int col = pos.getCol();
		
		return bgHexagons[row][col];
	}
	
	private Cell getCellAt(Cell[][] cells, Position pos){
		int row = pos.getRow();
		int col = pos.getCol();
		
		return cells[row][col];
	}
	
	private void drawAntDirectionArrow(Hexagon h, Cell c) {
		Ant a = c.getAnt();
		
		int xPos = h.getxPos();
		int yPos = h.getyPos();
		
		int sideLength = h.getSideLength();
		// draw direction line
		float r = - radians(60*a.getDirection().getNumber() - 90);
		int yEnd = (int) (yPos + sideLength * cos(r) / 2);
		int xEnd = (int) (xPos + sideLength * sin(r) / 2);
		
		stroke(0,255,0);
//			strokeWeight(2);
		line(xPos,yPos,xEnd,yEnd);
	}
	
	public boolean brainAndWorldAreCorrect(){
		return this.world.brainAndWorldAreCorrect();
	}
	
	@Override
	public void keyPressed(){
		run = true;
	}
	
	public void stop(){
		super.stop();
	}
	
	public static void main(String args[]) {
		PApplet.main(new String[] { "--present", "AntGameGUI" });
	}
}
