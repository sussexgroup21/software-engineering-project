import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;


public class AntWorld {

	private RandomNumberGenerator seed;
	private Cell[][] cells;
	private Ant[] ants;
	private int round;
	private ArrayList<Cell> foodHash = new ArrayList<Cell>(); //Needed for world parsing
	public int redFood = 0;
	public int blackFood = 0;
	private boolean brainAndWorldAreCorrect;
	private AntBrain blackBrain;
	private AntBrain redBrain;
	
	private static final int RANDOM_SEED = 12345;
	
	public AntWorld(String fileLoc, String redBrainPath, String blackBrainPath, boolean acceptAnything){
		this.readWorldFile(fileLoc);
		
		this.redBrain = new AntBrain(AntBrainParser.parseFile(redBrainPath));
		this.blackBrain = new AntBrain(AntBrainParser.parseFile(blackBrainPath));
		this.seed = new RandomNumberGenerator(RANDOM_SEED);
	}
	
	public AntWorld(String fileLoc, String redBrainPath, String blackBrainPath){
		System.out.println("CHECKING WORLD "+"'"+fileLoc+"'");
		
		boolean isWorldOK = this.readWorldFile(fileLoc) && this.checkWorld();
		System.out.println("World: "+(isWorldOK ? "OK" : "NOT OK"));
		
		boolean isRedBrainOK = AntBrainParser.checkFile(redBrainPath);
		System.out.println("Red Brain: "+(isRedBrainOK ? "OK" : "NOT OK"));
		
		boolean isBlackBrainOK = AntBrainParser.checkFile(blackBrainPath);
		System.out.println("Black Brain: "+(isBlackBrainOK ? "OK" : "NOT OK"));
		
		this.brainAndWorldAreCorrect = isWorldOK && isRedBrainOK && isBlackBrainOK;
		
		if(this.brainAndWorldAreCorrect){
			System.out.println("ALL THINGS ARE OK");
			this.redBrain = new AntBrain(AntBrainParser.parseFile(redBrainPath));
			this.blackBrain = new AntBrain(AntBrainParser.parseFile(blackBrainPath));
			this.seed = new RandomNumberGenerator(RANDOM_SEED);
		}
	}
	
	public boolean brainAndWorldAreCorrect(){
		return this.brainAndWorldAreCorrect;
	}
	
	/**
	 * Plays one round of the Ant Game - calls step for every alive ant
	 */
	int x =0;
	public void playRound(){
		for (int j=0; j<this.ants.length; j++){
			step(j);
		}
		
		System.out.println("Round:"+ x++ +"RedFood"+redFood+"BlackFood:"+blackFood);
		round++;
	}
	
	public void playRounds(int numRounds){
	//	System.out.println("random seed: 12345\n");
		for (int i=0; i< numRounds; i++){
	//		System.out.println("After round "+i+"...");
	//		this.printInfo();	
			this.playRound();		
	//		System.out.println();
		}
	}
	
	public void printInfo(){
		for(int row=0; row<this.cells.length; row++){
			for (int col=0; col<this.cells[row].length; col++){		
				System.out.println(this.cells[row][col]);
			}
		}
	}
	
	public void showCells(){
		for (int row=0; row<this.cells.length; row++){			
			for (int col=0; col<this.cells[0].length; col++){
				System.out.print(this.cells[row][col]);
			}
			if(row < this.cells.length-1){ //print new line after each row (Except last row)
				System.out.println();
			}
		}
	}
	
	/**
	 * Returns true if given cell contains ant
	 * False if not.
	 * @return
	 */
	public Boolean antIsAt(Position pos) {
		return this.cells[pos.getRow()][pos.getCol()].hasAnt();
		
	}
	

	/**
	 * returns ant at given coordinates
	 * @param pos
	 * @return
	 */
	public Ant antAt(Position pos) {
		return this.cells[pos.getRow()][pos.getCol()].getAnt();
	}
	
	/**
	 * Set ant at given coordinates
	 * @param pos
	 * @param a
	 * @return
	 */
	public void setAntAt(Position pos,Ant a) {
		this.cells[pos.getRow()][pos.getCol()].setAnt(a);
		a.setPosition(pos);
	}
	
	/**
	 * kills ant with given id
	 * @param id
	 * @return
	 */
	public void killAnt(int id) {
		if (this.ants[id].isRed()){
			//this.redCasualties++;
		} else {
			//this.blackCasualties++;
		}
		this.ants[id] = null;
	}
	/**
	 * kills ant at given position
	 * @param pos
	 * @return
	 */
	public void killAntAt(Position pos) {
		Ant a = this.cells[pos.getRow()][pos.getCol()].getAnt();
		if(a != null){
			int id = a.getId();
			killAnt(id);
			this.clearAntAt(pos);
			
			System.out.println(a +" killed at "+pos);			
	//		this.printAnts();
		}
	}
	
	public void printAnts(){
		System.out.println("Ants:");
		for (int i = 0; i < this.ants.length; i++) {
			System.out.println(i+": ["+this.ants[i]+"]");
		}
	}
	
	/**
	 * returns true if ant with given id is alive
	 * @param id
	 * @return
	 */
	public boolean antIsAlive(int id) {

		return this.ants[id] != null;
	}
	
	/**
	 * return the amount of food at given position
	 * @param pos
	 * @return
	 */
	public int foodAt(Position pos) {
		return this.cells[pos.getRow()][pos.getCol()].getFood();
	}
	
	/**
	 * set the amount of food at given position to i
	 * @param pos
	 * @return
	 */
	public void setFoodAt(Position pos, int i) {
		Cell hillCell = this.cells[pos.getRow()][pos.getCol()];
		if(hillCell.getCellType().equals("red hill")){
			this.redFood = this.redFood - hillCell.getFood() + i; //update foodCount
		}else if (hillCell.getCellType().equals("black hill")){
			this.blackFood = this.blackFood - hillCell.getFood() + i;
		}
		this.cells[pos.getRow()][pos.getCol()].setFood(i);
	}
	
	/**
	 * returns true if pos contains ant hill of given colour
	 * @param pos
	 * @param isRed
	 * @return
	 */
	public boolean anthillAt(Position pos, boolean isRed) {
		String hillName = (isRed ? "red" : "black").concat(" hill");
		return this.cells[pos.getRow()][pos.getCol()].cellType().equals(hillName);
	}
	
	/**
	 * set marker at given position
	 * @param pos
	 * @param colour
	 * @param marker
	 */
	public void setMarkerAt(Position pos, boolean isRed, int marker) {
		this.cells[pos.getRow()][pos.getCol()].setMarker(isRed, marker);
	}

	/**
	 * set marker at given position
	 * @param pos
	 * @param colour
	 * @param marker
	 */
	public void clearMarkerAt(Position pos, boolean isRed, int marker) {
		this.cells[pos.getRow()][pos.getCol()].clearMarker(isRed, marker);
	}
	/**
	 * returns true if marker i of colour c is set in cell p
	 * @param pos
	 * @param colour
	 * @param marker
	 * @return
	 */
	public boolean checkMarkerAt(Position pos, boolean isRed, int marker) {
		return this.cells[pos.getRow()][pos.getCol()].getMarkers(isRed)[marker];
		
	}
	/**
	 * true if ANY marker of color c is set in cell p
	 * @param pos
	 * @param isRed
	 * @return
	 */
	public boolean checkAnyMarkerAt(Position pos, boolean isRed) {
		boolean[] markers = this.cells[pos.getRow()][pos.getCol()].getMarkers(isRed);
		for(boolean b : markers) if(!b) return true;
	    return false;
	}
	
	/**
	 * returns true if cell at pos is rocky 
	 * @param pos
	 * @return
	 */
	public boolean rocky(Position pos) {
		return this.cells[pos.getRow()][pos.getCol()].getCellType().equals("rock");
	}
	
	/**
	 * The function cell_matches takes a position p, a condition cond, and a color c 
	 * (the colour of the ant that is doing the sensing), and checks whether cond holds at p.
	 * @param pos
	 * @param condition
	 * @param isRed - is this current ant red? 
	 * @return
	 */
	public boolean cellMatches(Position pos, String condition, boolean isRed) {
		String[] args = condition.split(" ");

		CondEnum cond = CondEnum.valueOf(args[0]);

		switch (cond) {
		case Rock:
			return rocky(pos);
		case Friend:
			// there is a same coloured ant at pos
			return antIsAt(pos) && antAt(pos).isRed() == isRed;
		case Foe:
			// there is a different coloured ant at pos
			return antIsAt(pos) && !antAt(pos).isRed() == isRed;
		case FriendWithFood:
			// there is a same coloured ant at pos with food
			return antIsAt(pos) && antAt(pos).isRed() == isRed
					&& antAt(pos).hasFood();
		case FoeWithFood:
			// there is a different coloured ant at pos with food
			return antIsAt(pos) && !antAt(pos).isRed() == isRed
					&& antAt(pos).hasFood();
		case Food:
			// food at pos?
			return this.cells[pos.getRow()][pos.getCol()].getFood() > 0;
		case FoeMarker:
			return checkAnyMarkerAt(pos, !isRed);
		case Home:
			return anthillAt(pos, isRed);
		case FoeHome:
			return anthillAt(pos, !isRed);
		case Marker:
			int markerNum = Integer.parseInt(args[1]);
			return checkMarkerAt(pos, isRed, markerNum);
		default:
			return false;
		}
	}

	/**
	 * returns the coords in the form a Position obj of the next cell
	 * in direction dir.
	 * @param pos
	 * @param dir
	 * @return
	 */
	public Position adjacentCell(Position pos, Direction dir ) {
		
		//get current pos from pos object.
		int col = pos.getCol();
		int row = pos.getRow();
		switch(dir){
		case EAST: //0
			col++; // move east/right
			break;
		case SOUTHEAST: //1
			if(row % 2 == 0){ //if row is even
				row++; col++; // increase both
			}else{ //row is odd
				row++; //increase row
			}
			break;
		case SOUTHWEST: //2
			if(row % 2 == 0){ //if row is even
				row++; // increase row
			}else{ //row is odd
				row++; col--; //increase row, but decrease column	
			}
			break;
		case WEST: //3
			col--; //move left/west
			break;
		case NORTHWEST: //4
			if(row % 2 == 0){ //if row is even
				row--; // Decrease row
			}else{ //row is odd
				row--; col--; //decrease both
			}
			break;
		case NORTHEAST: //5
			if(row % 2 == 0){ //if row is even
				row--; col++; // Decrease row, increase col
			}else{ //row is odd
				row--; // decrease row
			}
			break;	
		}
		
		Position adjCell = new Position(row, col);
		
		return adjCell;
	}
	
	public List<Position> adjacentCells(Position pos){
		Direction[] directions = Direction.values();
		ArrayList<Position> output = new ArrayList<Position>();
		
		for (int i = 0; i < directions.length; i++) {
			Position p = this.adjacentCell(pos, directions[i]);
			if(this.inGrid(p)){
				output.add(p);
			}
		}
		
		return output;
	}
	
	public boolean inGrid(Position p){
		return  p.getCol()>= 0 && 
				p.getCol() < this.cells[0].length && 
				p.getRow() >= 0 && 
				p.getRow() < this.cells.length;
	}
	
	/**
	 * returns number of ants of given colour adj to pos
	 * @param pos
	 * @param enemyColour
	 * @return
	 */
	public int numOfAdjacentAnts(Position pos, boolean enemyIsRed) {
		int antCount = 0;
		// System.out.println(pos);
		for (Direction dir : Direction.values()) {
			 Position nextCell = this.adjacentCell(pos, dir);
			 // System.out.println("\t"+nextCell);
			 //there is an ant and that ant is of given colour
			 if(this.antIsAt(nextCell) && (this.antAt(nextCell).isRed() == enemyIsRed)){
				 antCount ++; //increment adj ants
			 }
			}
		return antCount;	
	}
	
	/**
	 * check adj cells to pos and kills ant at pos if surrounded
	 * @param pos
	 */
	public void checkForSurroundedAntAt(Position pos) {
		if(this.antIsAt(pos)){
			Ant a = this.antAt(pos);
			//Surrounded by at least 5 enemy ants
			if(this.numOfAdjacentAnts(pos, !a.isRed()) >= 5){
				this.killAntAt(pos);
				//System.out.println("Ant at "+pos+" has been killed!");
				this.setFoodAt(pos, 3); // dead ant becomes 3 food :(
			}
		}
	}
	
	/**
	 * Perform action on ant based on given instruction updates ant state and world
	 * to reflect action.
	 * @param id
	 */
	public void step(int id) {
		if(this.antIsAlive(id)){			
			Ant a = this.ants[id];
			Position pos = a.getPos();
			if(a.getResting() > 0){
				a.setResting(a.getResting() - 1);
			}
			else{
				Instruction ins = null;
				
				this.checkForSurroundedAntAt(a.getPos()); //kill ant?
				
				if(a.isRed()){
					ins = this.redBrain.getInstruction(a.getState());
				}else{
					ins= this.blackBrain.getInstruction(a.getState());
				}
				
				// evaluate the ant's instruction
				if(ins instanceof Sense){
					Sense sen = (Sense) ins;
					Position sensedPos = this.sensedCell(pos, a.getDirection(), (sen.senseDir()));
					if(this.cellMatches(sensedPos,sen.condition(), a.isRed())){
						a.setState(sen.nextState()); //st1
					}else{
						a.setState(sen.failState()); //st2
					}
					
				}else if(ins instanceof Mark){
					Mark mar = (Mark) ins;
					this.setMarkerAt(pos, a.isRed(), mar.marker());
					a.setState(mar.nextState());	
					
				}else if(ins instanceof Unmark){
					Unmark unmar = (Unmark) ins;
					this.clearMarkerAt(pos, a.isRed(), unmar.marker());
					a.setState(unmar.nextState());
					
				}else if(ins instanceof PickUp){
					PickUp pu = (PickUp) ins;
		            if(a.hasFood() || this.foodAt(pos) == 0){ //no food or already carrying food
		            	a.setState(pu.failState()); // can't pick up food
		            }else{
		            	this.setFoodAt(pos, this.foodAt(pos) -1); // food in cell -1
		            	a.setHasFood(true);
		            	a.setState(pu.nextState());
		            }					
		            
				}else if(ins instanceof Drop){
					Drop dr = (Drop) ins;
					if(a.hasFood()){
						this.setFoodAt(pos, this.foodAt(pos) +1); // food in cell +1
		            	a.setHasFood(false);
					}//else do nothing
					a.setState(dr.nextState());
					
					
				}else if(ins instanceof Turn){
					Turn tur = (Turn) ins;				
					a.setDirection(Direction.turn(tur.getDirection(), a.getDirection())); //turn ant to left or right
					a.setState(tur.nextState());
					
				}else if(ins instanceof Move){
					Move mov = (Move) ins;
					Position adjPos = this.adjacentCell(pos, a.getDirection());
					if(this.rocky(adjPos) || this.antIsAt(adjPos)){
						a.setState(mov.failState()); // can't move
					}else{ //clear cell(no ant or rock)
						this.clearAntAt(pos); // clear ant from old pos
						this.setAntAt(adjPos, a);
						a.setState(mov.nextState()); //success, ant moved.
						a.setResting(14);//must now rest for 14 turns
//						this.checkForSurroundedAntAt(adjPos); //kill ant?
					}
					
				}else if(ins instanceof Flip){
					Flip fli = (Flip) ins;
					if(this.seed.randomint(fli.p()) == 0){
						a.setState(fli.nextState());
					}else{
						a.setState(fli.failState());
					}
				}
			}
		}
    	
	}
	
	/**
	 * set ant in cell to null
	 * @param pos
	 */
	private void clearAntAt(Position pos) {
		this.cells[pos.getRow()][pos.getCol()].clearAnt();
	}
	/**
	 * returns the position of the cell being sensed.
	 * @param pos
	 * @param dir
	 * @param senseDir
	 * @return
	 */
	private Position sensedCell(Position pos, Direction dir, String senseDir) {
		SenseDirEnum s = SenseDirEnum.valueOf(senseDir);
		
		switch(s){
			case Here:
				return pos;
			case Ahead:
				return this.adjacentCell(pos, dir);
			case LeftAhead:
				return this.adjacentCell(pos, Direction.turn("Left",dir));
			case RightAhead:
				return this.adjacentCell(pos, Direction.turn("Right",dir));
		}
		return null;
	}
	
	public Cell[][] getCells(){
		return this.cells;
	}
	
	public int getRound(){
		return round;
	}
	
	public static void main(String[] args){
		AntWorld a = new AntWorld("\\worlds\\sample.world","\\brains\\dumb-brain.brain","\\brains\\sample.brain");
		if(a.brainAndWorldAreCorrect()){
			a.playRounds(1000);
		} else {
			System.out.println("ERROR WITH BRAINS OR WORLD");
		}
	}
	
	public boolean readWorldFile(String fileLoc){
		Scanner scanner = null;
		ArrayList<Ant> tempAnts = new ArrayList<Ant>();
		int antID = 0;
		try { 
			File file = new File(fileLoc);
            scanner = new Scanner(file);
			this.cells = new Cell[Integer.parseInt(scanner.nextLine())][Integer.parseInt(scanner.nextLine())];
            int row = 0;
			while (scanner.hasNextLine()) {
				int col = 0;
				String[] line = scanner.nextLine().trim().split(" ");
                for(String cellSpecifier : line){
					if(cellSpecifier.equals("#")){
						this.cells[row][col] = new Cell(row, col, "rock");
					}					else if(cellSpecifier.equals(".")){//Clear Cell
						this.cells[row][col] = new Cell(row, col, "clear");
					}
					else if(cellSpecifier.equals("+")){// Red Anthill
						// Create red ant at this position
               			Ant newAnt = new Ant(antID,true,new Position(row,col));
						this.cells[row][col] = new Cell(row, col, "red hill",newAnt);
						tempAnts.add(newAnt);
						antID++;
					}
					else if(cellSpecifier.equals("-")){//black Anthill
						// Create black ant at this position
						Ant newAnt2 = new Ant(antID,false,new Position(row,col));
               			this.cells[row][col] = new Cell(row, col, "black hill",newAnt2);
						tempAnts.add(newAnt2);
						antID++;
					}
					else if(cellSpecifier.equals("5")){
						int foodAmount = Integer.parseInt(line[col]);
               			this.cells[row][col] = new Cell(row, col, "food", foodAmount);
					} else {
						System.out.println("[WORLD ERROR] Illegal cell specifier '"+cellSpecifier+"'"+" at "+ new Position(row,col));
						return false;
					}
               		col++;
                }
				row++;
			}					
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
        	ants = tempAnts.toArray(new Ant[0]);
        	scanner.close();
        }
		return true;
	}
	
	public boolean checkWorld(){
		int blobCount = 0;
		int rockCount = 0;
		boolean containsRedHill = false;
		boolean containsBlackHill = false;

		if(!checkPerimeter()){
			System.out.println("[WORLD ERROR] World perimeter must be rocks");
			return false;
		}

		for (int row=1;row<cells.length-1;row++){
			for (int col =1;col<cells[row].length-1;col++){
				if (this.cells[row][col].getFood()==5&&!this.foodHash.contains(cells[row][col])){
					if (!checkBlob(new Position(row,col))){
						System.out.println("[WORLD ERROR] Incorrect food blob at "+ new Position(row,col));
						return false;
					} else {
						blobCount++;
					}
				} else if (cells[row][col].getCellType().equals("rock")){
					if(!checkRock(new Position(row,col))){
						System.out.println("[WORLD ERROR] Incorrect rock at "+ new Position(row,col));
						return false;
					} else {
						rockCount++;
					}
				} else if (!containsRedHill && cells[row][col].getCellType().equals("red hill")){
					if(!checkHill(new Position(row,col), "red hill")){
						System.out.println("[WORLD ERROR] Incorrect red hill at "+ new Position(row,col));
						return false;
					} else {
						containsRedHill = true;
					}
				}  else if (!containsBlackHill && cells[row][col].getCellType().equals("black hill")){
					if(!checkHill(new Position(row,col),"black hill")){
						System.out.println("[WORLD ERROR] Incorrect black hill at "+new Position(row,col));
						return false;
					} else {
						containsBlackHill = true;
					}
				}
			}
		}
		if(blobCount != 11 || rockCount != 14 || !containsRedHill || !containsBlackHill){
			System.out.println("[WORLD ERROR] World must have 11 food blobs, 14 rocks, 1 red and 1 black hill. (blobs:"+blobCount+" rocks:"+rockCount+" redHill:"+containsRedHill+" blackHill:"+containsBlackHill+")" );
			return false;
		}
		return true;
	}

	public boolean checkHill(Position p,String hillType){
		Position startPos = adjacentCell(p,Direction.NORTHWEST);		
		int i = 8;
		while (i >= 0){
			i--;
		//	System.out.println(i);
			Position nextPos;
			if(i==7){
				nextPos = startPos;
			} else {
				nextPos = adjacentCell(startPos, Direction.SOUTHEAST);
				startPos = nextPos;
			}
			if (i==0){
				if(!this.cells[nextPos.getRow()][nextPos.getCol()].getCellType().equals(hillType)){
			//		System.out.println("Center cell is not "+hillType);
					return false;	
				}						
			} else {
				for (int currentSideOfAntHill=0;currentSideOfAntHill<6;currentSideOfAntHill++){ //0: N side; 1: NE side; 2: SE side; 3: S side; 4: SW side; 5: NW side
					for(int j=0; j<i; j++){
				//		System.out.println("i:"+i+" side:"+currentSideOfAntHill+nextPos +" has: "+this.cells[nextPos.getRow()][nextPos.getCol()].getCellType());
						if (i==7 && !this.cells[nextPos.getRow()][nextPos.getCol()].getCellType().equals("clear")){							
					//		System.out.println("fails at "+nextPos);
					//		System.out.println("not clear");
							return false;
						} else if (i<7 && !this.cells[nextPos.getRow()][nextPos.getCol()].getCellType().equals(hillType)){
					//		System.out.println("not "+hillType);
							return false;
						} else {
							switch(currentSideOfAntHill) {
							case 0:
								nextPos = adjacentCell(nextPos,Direction.EAST); break;
							case 1:
								nextPos = adjacentCell(nextPos,Direction.SOUTHEAST); break;
							case 2:
								nextPos = adjacentCell(nextPos,Direction.SOUTHWEST); break;
							case 3:
								nextPos = adjacentCell(nextPos,Direction.WEST); break;
							case 4:
								nextPos = adjacentCell(nextPos,Direction.NORTHWEST); break;
							case 5:
								nextPos = adjacentCell(nextPos,Direction.NORTHEAST); break;
							}				
						}
					}
				}
			}
//			System.out.print(i);
		}
	//	System.out.println("Finished scan successfully?");
		return true;
	}
	
	public boolean checkRock(Position p){
		for(Position adjacent : adjacentCells(p)){ // check all adjacent cells for non-clear cells
			if (!cells[adjacent.getRow()][adjacent.getCol()].getCellType().equals("clear")){
				return false;
			}
		}
		return true;
	}
	
	public boolean checkPerimeter(){
		for (int col=0; col<cells[0].length; col++){
			if(!cells[0][col].getCellType().equals("rock") || !cells[cells.length-1][col].getCellType().equals("rock")){
				return false;
			}
		}		
		for (int row=0; row<cells.length; row++){
			if(!cells[row][0].getCellType().equals("rock") || !cells[row][cells.length-1].getCellType().equals("rock")){
				return false;
			}
		}		
		return true;
	}
	

	public boolean checkBlob(Position p){
		
		int blobRow = p.getRow(); // coordinates of the first food(5) in the blob. If the food is not part of a blob, this method will return false
		int blobCol = p.getCol();
		
		for (int row = blobRow-1; row<blobRow+6; row++){ 
			for (int col =blobCol-1;col<blobCol+6;col++){ //iterate over the blob as well as the cells around blob
				if (row == blobRow-1||row==blobRow+5||col==blobCol-1||col==blobCol+5){ //cells around blob (note: some of these cells are actually NOT surrounding the blob due to hexagonal grid)
					if (blobRow%2 == 0){ //Blob starts on an even row
						if (!((row==blobRow-1 && col==blobCol-1)||(row==blobRow+5 && col==blobCol-1))){ //Disregard top left and bottom left cells which would not surround blob on a hex grid
							if (!cells[row][col].getCellType().equals("clear")){ //check all other surrounding cells if they are clear
								return false;
							}
						}
					} else { //blob starts on an odd row
						if (!((row==blobRow-1 && col==blobCol+5)||(row==blobRow+5 && col==blobCol+5))){ //Disregard top right and bottom right cells which would not surround blob on a hex grid
							if (!cells[row][col].getCellType().equals("clear")){//check all other surrounding cells if they are clear
								return false;
							}
						}
					}
				} else {//the food blob
					if (cells[row][col].getFood()!=5){//checks that all cells in blob are actually food(5)
						return false;
					}
					this.foodHash.add(cells[row][col]);//adds cell to hashset to make sure that this blob does not get checked again
				}
			}
		}
		return true;
	}
	public String checkComplete(){
		int aliveRed=0;
		int aliveBlack=0;
		for (Ant ant:ants){
			if (ant!=null){
				if (ant.isRed()){
					aliveRed++;
				} else{
					aliveBlack++;
				}
			}
		}
		if (aliveRed==0){
			return "blackWin";
		}
		if (aliveBlack==0){
			return "redWin";
		}
		return "running";
	}
	
	public int getRedNumAnts(){
		int aliveRed=0;
		for (Ant ant:ants){
			if (ant!=null && ant.isRed()){
				aliveRed++;
			}
		}
		return aliveRed;
	}
	
	public int getBlackNumAnts(){
		int aliveRed=0;
		for (Ant ant:ants){
			if (ant!=null && !ant.isRed()){
				aliveRed++;
			}
		}
		return aliveRed;
	}
	
	public int getBlackFood(){
		return this.blackFood;
	}
	
	public int getRedFood(){
		return this.redFood;
	}
}
