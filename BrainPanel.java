import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BrainPanel extends JPanel{
	String brainName; 
	int score=0;
	JTextField jbScore;
	JButton jbBrowse; 
	
	public BrainPanel(String brainName, final JPanel jpDisplayBrains, final TournamentPanel jfTourn, final ArrayList<BrainPanel> alTournamentBrains, int width, int height) {
		
		this.brainName = brainName;
		jbBrowse = new JButton();
		jbBrowse.setText("Browse");
		jbBrowse.setPreferredSize(new Dimension((int) (width*.1),(int)(height*.065)));
		this.add(jbBrowse);
		
		final JTextField jbName = new JTextField();
		jbName.setPreferredSize(new Dimension((int) (width*.2),(int)(height*.065)));
		jbName.setText(brainName);
		this.add(jbName);
		
		jbScore = new JTextField();
		jbScore.setPreferredSize(new Dimension((int) (width*.1),(int)(height*.065)));
		jbScore.setText("0");
		jbScore.setEditable(false);
		this.add(jbScore);
		
		JButton jbRemove = new JButton();
		jbRemove.setText("X");
		//jbRemove.setPreferredSize(new Dimension((int) (width*.1),(int)(height*.065)));
		this.add(jbRemove);

		jbRemove.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				alTournamentBrains.remove(BrainPanel.this);
				jpDisplayBrains.remove(BrainPanel.this);
				jpDisplayBrains.repaint();
				jfTourn.pack();
			}
		});
		
		final JFileChooser jfc = new JFileChooser("./brains");
		
		jbBrowse.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = jfc.showOpenDialog(BrainPanel.this);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					BrainPanel.this.brainName = jfc.getSelectedFile().getPath();
					jbName.setText(BrainPanel.this.brainName);
				}
				jfTourn.pack();
			}
		});
	}
	public void increaseScore(){
		this.score=score+1;
		System.out.println("Increaseing score"+this.score);
		this.jbScore.setText(this.score+"");
	}
}