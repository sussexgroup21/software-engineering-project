/***
 * 
 * @author mw277
 * @version 1.0.0
 */
public class Cell {
	private Ant ant; 
	private String cellType;
	private int row;
	private int col;
	private boolean[] redMarkers = new boolean[6];
	private boolean[] blackMarkers = new boolean[6];
	private int food;
	
	/**
	 * @param row 
	 * @param col
	 * @param cellType
	 */
	public Cell(int row,int col,String cellType){
		this.row = row;
		this.col = col;
		this.cellType = cellType;
		this.ant = null;
		this.food = 0;
	}
	/**
	 * food constructor
	 * @param row
	 * @param col
	 * @param cellType
	 * @param f
	 */
	public Cell(int row,int col,String cellType,int f){
		this.row = row;
		this.col = col;
		this.cellType = cellType;
		this.ant = null;
		this.food = f;
	}
	public Cell(int row,int col,String cellType,Ant a){
		this.row = row;
		this.col = col;
		this.cellType = cellType; 
		this.food = 0;
		this.ant = a;
	}
	
	/**
	 * @return 
	 * true if marker in position i 
	 * else false
	 */
	public boolean containsMarker(boolean isRed, int i){
		return isRed ? redMarkers[i] : blackMarkers[i];
	}
	/**
	 * Clears marker in position i 
	 * @param i
	 */
	public void clearMarker(boolean isRed, int i){
		if(isRed){
			redMarkers[i] = false;
		}else{
			blackMarkers[i] = false;
		}
	}
	/**
	 * Sets marker in position i
	 * @param i
	 */
	public void setMarker(boolean isRed, int i){
		if(isRed){
			redMarkers[i] = true;
		}else{
			blackMarkers[i] = true;
		}
	}
	/**
	 * Clears all markers
	 * @param i
	 */
	public void clearAllMarkers(){
		for (int i = 0; i < this.redMarkers.length; i++) {
			this.redMarkers[i] = false;
			this.blackMarkers[i] = false;
		}
	}
	/**
	 * 
	 * @return cellType
	 */
	public String cellType(){
		return cellType;
	}
	/**
	 * returns the Ant currently in this cell
	 * @return
	 */
	public Ant getAnt() {
		return ant;
	}
	/**
	 * Inputs ant into this cell
	 * @param containsAnt
	 */
	public void setAnt(Ant ant) {
		this.ant = ant;
	}
	
	public void clearAnt() {
		ant = null;
	}
	
	public boolean hasAnt(){
		return ant != null;
	}
	
	/**
	 * Returns marker array of this cell
	 * @return
	 */
	public boolean[] getMarkers(boolean isRed) {
		return isRed ? redMarkers : blackMarkers;
	}
	
	public boolean hasAnyMarkers(){
		int i = 0;
		while(i < this.redMarkers.length && !redMarkers[i] && !blackMarkers[i]){
			i++;
		}
		return i < this.redMarkers.length;
	}
	
	public int getRow(){
		return row;
	}
	
	public int getCol(){
		return col;
	}
	
	public String getCellType(){
		return cellType;
	}
	
	public int getFood() {
		return this.food;
		
	}
	
	public void setFood(int i) {
		this.food = i;
		if(i <= 0 && this.cellType.equals("food")){
			this.cellType = "clear";
		} else if(i > 0 && this.cellType.equals("clear")){
			this.cellType = "food";
		}
	}
	

	@Override
	public String toString(){
		String out;
	
		if (this.cellType.equals("food") || this.cellType.equals("clear")) { 
			out = "cell ("+this.row+", "+this.col+"):"; //dont show food or clear celltype in output
		} else {
			out = "cell ("+this.row+", "+this.col+"): "+cellType+";";  //show cell type of other cells
		}
		
		if (this.food >0){
			out += " "+food + " food;";
		}
		String rMarks ="";
		for(int i=0; i<6; i++){
			if(redMarkers[i]){
				rMarks += i;
			}
		}
		
		String bMarks ="";					
		for(int i=0; i<6; i++){
			if(blackMarkers[i]){
				bMarks += i;
			}
		}
					
		if (!rMarks.equals("")){
			out += " red marks: "+rMarks+";";
		}
		if (!bMarks.equals("")){
			out += " black marks: "+bMarks+";";
		}
		
		if (this.hasAnt()){
			Ant a = this.ant;
			int food;
			if (a.hasFood()){
				food = 1;
			} else { food = 0; }
			String colorString = a.isRed() ? "red" : "black";
			out += " "+colorString+" ant of id " +a.getId()+", dir "+a.getDirection()+", food "+food+", state "+a.getState()+", resting "+a.getResting();		
		}
		
		return out;
	}
}
