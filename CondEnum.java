
public enum CondEnum {
	Friend,
	Foe,
	FriendWithFood,
	FoeWithFood,
	Food,
	Rock,
	FoeMarker,
	Home,
	FoeHome,
	Marker;
}
