
public enum Direction {

	//have given each dir an number may be of use??
	EAST(0),
	SOUTHEAST (1),
	SOUTHWEST (2),
	WEST (3),
	NORTHWEST (4),
	NORTHEAST(5);
	
	private int number;
	
	private Direction(int number) {
	    this.number = number;
	 }
	
	 public int getNumber() {
	     return number;
	 }
	  
	 public static Direction turn(String lr, Direction dir){
		if (lr.equals("Right")){
			int newDir = (dir.getNumber() + 1) % 6;
			return Direction.values()[newDir];
		} else if (lr.equals("Left")){
			int newDir = ((((dir.getNumber()-1) %6) +6) %6);
			return Direction.values()[newDir];
		} else {
			throw new IllegalArgumentException("lr must be either 'Right' or 'Left'!");
		}
	 }
}
