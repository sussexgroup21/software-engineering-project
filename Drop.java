public class Drop extends Instruction {
	int s;
	
	public Drop(String i, String state){
		super(i);
		this.s = Integer.parseInt(state);
	}	
	
	public int nextState(){
		return this.s;
	}
}