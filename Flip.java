public class Flip extends Instruction {
	int failState;
	int successState;
	int p;
	
	public Flip(String i, String p, String st1, String st2){
		super(i);
		this.failState = Integer.parseInt(st2);
		this.successState = Integer.parseInt(st1);
		this.p = Integer.parseInt(p);
	}
	
	public int nextState(){
		return this.successState;
	}
	public int failState(){
		return this.failState;
	}	
	public int p(){
		return this.p;
	}
}