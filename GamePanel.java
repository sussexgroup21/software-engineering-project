import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JViewport;


public class GamePanel extends JFrame {
	private int width;
	private int height;
	private int sideWidth;
	
	private AntGameGUI antGameGui;
	
	private JViewport antGameViewport;
	
	private String currentWorldPath;
	private String currentRedBrainPath;
	private String currentBlackBrainPath;
	
//	public static JTextField jtfActivity;
	public static JTextField jtfRedScore;
	public static JTextField jtfBlackScore;
	
	public static JTextField jtfRedNumAnts;
	public static JTextField jtfBlackNumAnts;
	
	public GamePanel(int width, int height){
		this.width = width;
		this.height = height;
		this.sideWidth = (int) (width*.3);
		
		this.setResizable(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.add(mainPanel());
		
		this.setVisible(true);
		
		this.pack();
	}
	
	private JPanel mainPanel(){
		//Splits main Jframe into two Jpanels		
		JPanel mainPanel = new JPanel();
		BoxLayout b = new BoxLayout(mainPanel,BoxLayout.X_AXIS);
		mainPanel.setLayout(b);
		
		mainPanel.add(leftPanel());
		mainPanel.add(rightPanel());
		
		return mainPanel;
	}
	
	private JPanel rightPanel(){
		final JPanel rightPanel = new JPanel();
 
		rightPanel.setPreferredSize(new Dimension(width,height));
		
		rightPanel.setBackground(Color.YELLOW);
		
		rightPanel.setLayout(new BorderLayout());
		
		final ScrollPane sGamePane = new ScrollPane(ScrollPane.SCROLLBARS_ALWAYS);
		sGamePane.setWheelScrollingEnabled(true);
		
		antGameViewport = new JViewport();
		sGamePane.add(antGameViewport);
		
		rightPanel.add(sGamePane,BorderLayout.CENTER);
		return rightPanel;
	}
	
	private JPanel leftPanel(){
		final JPanel leftPanel = new JPanel();
		
		leftPanel.setPreferredSize(new Dimension((int) (sideWidth),height));

//		leftPanel.setBackground(Color.RED);
		
		// home button
		JButton jbHome = new JButton();
		jbHome.setPreferredSize(new Dimension((int) (sideWidth),(int)(height*.2)));
		jbHome.setText("Home");
		Font font =new Font("Verdana", Font.BOLD, 32); //TextSize and font
		jbHome.setFont(font);
//		jbHome.setBackground(Color.GREEN);
		leftPanel.add(jbHome);
		
		//Back to homeScrren
		jbHome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//processingThread.destroy();
				resetGUI();
				HomePanel hp = new HomePanel(width, height);
				GamePanel.this.dispose();
			}
		});
		
		//Select world Panel
		JPanel jpSelectWorld = new JPanel();
//		jpSelectWorld.setBackground(Color.GREEN);
		jpSelectWorld.setPreferredSize(new Dimension((int) (sideWidth),(int)(height*.10)));
		
		JLabel jlSelectWorld = new JLabel("World:");
		jlSelectWorld.setPreferredSize(new Dimension((int) (sideWidth*.7),(int)(height*.04)));
		jpSelectWorld.add(jlSelectWorld);
		
		JButton jbRandomWorld = new JButton();
		jbRandomWorld.setText("?");
		jbRandomWorld.setPreferredSize(new Dimension((int) (sideWidth*.2),(int)(height*.04)));
		jpSelectWorld.add(jbRandomWorld);
		
		JButton jbBrowseWorld = new JButton();
		jbBrowseWorld.setText("Browse"); 
		jbBrowseWorld.setPreferredSize(new Dimension((int) (sideWidth*.4),(int)(height*.04)));
		jpSelectWorld.add(jbBrowseWorld);
		final JTextField jtfBrowseWorld = new JTextField();
		jtfBrowseWorld.setPreferredSize(new Dimension((int) (sideWidth*.5),(int)(height*.04)));
		jtfBrowseWorld.setText(currentWorldPath);
		jpSelectWorld.add(jtfBrowseWorld);
		
		
		leftPanel.add(jpSelectWorld);

		final JFileChooser worldFc = new JFileChooser("./worlds");
		
		// browse (world)
		jbBrowseWorld.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = worldFc.showOpenDialog(leftPanel);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					setCurrentWorldPath(worldFc.getSelectedFile().getPath());
					jtfBrowseWorld.setText(currentWorldPath);
				}
			}
		});

		
		//Select Red Brain panel
		JPanel jpSelectRedBrain = new JPanel();
//		jpSelectRedBrain.setBackground(Color.GREEN);
		jpSelectRedBrain.setPreferredSize(new Dimension((int) (sideWidth),(int)(height* .10)));
		
		JLabel jlSelectRedBrain = new JLabel("Red Brain:");
		jlSelectRedBrain.setPreferredSize(new Dimension((int) (sideWidth *.9), (int) (height * .04)));
		jpSelectRedBrain.add(jlSelectRedBrain);
			
		JButton jbBrowseRedBrain = new JButton();
		jbBrowseRedBrain.setPreferredSize(new Dimension((int) (sideWidth*.4),(int)(height*.04)));
		jbBrowseRedBrain.setText("Browse"); 
		jpSelectRedBrain.add(jbBrowseRedBrain);
		final JTextField jtfBrowseBrain = new JTextField();
		jtfBrowseBrain.setPreferredSize(new Dimension((int) (sideWidth*.5),(int)(height*.04)));
		jtfBrowseBrain.setText(currentRedBrainPath);
		jpSelectRedBrain.add(jtfBrowseBrain);
		leftPanel.add(jpSelectRedBrain);
		
		final JFileChooser brainFc = new JFileChooser("./brains");
		
		// browse (red brain)
		jbBrowseRedBrain.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = brainFc.showOpenDialog(leftPanel);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					setCurrentRedBrainPath(brainFc.getSelectedFile().getPath());
					jtfBrowseBrain.setText(currentRedBrainPath);
				}
			}
		});
		
		
		//Select Black Brain panel
		JPanel jpSelectBlackBrain = new JPanel();
//		jpSelectBlackBrain.setBackground(Color.GREEN);
		jpSelectBlackBrain.setPreferredSize(new Dimension((int) (sideWidth),(int)(height*.10)));
		
		JLabel jlSelectBlackBrain = new JLabel("Black Brain:");
		jlSelectBlackBrain.setPreferredSize(new Dimension((int) (sideWidth *.9), (int) (height * .04)));
		jpSelectBlackBrain.add(jlSelectBlackBrain);
		
		JButton jbBrowseBlackBrain = new JButton();
		jbBrowseBlackBrain.setPreferredSize(new Dimension((int) (sideWidth*.4),(int)(height*.04)));
		jbBrowseBlackBrain.setText("Browse"); 
		jpSelectBlackBrain.add(jbBrowseBlackBrain);
		final JTextField jtfBrowseBrain2 = new JTextField();
		jtfBrowseBrain2.setPreferredSize(new Dimension((int) (sideWidth*.5),(int)(height*.04)));
		jtfBrowseBrain2.setText(currentBlackBrainPath);
		jpSelectBlackBrain.add(jtfBrowseBrain2);
		leftPanel.add(jpSelectBlackBrain);
		
		// browse (black brain)
		jbBrowseBlackBrain.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = brainFc.showOpenDialog(leftPanel);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					setCurrentBlackBrainPath(brainFc.getSelectedFile().getPath());
					jtfBrowseBrain2.setText(currentBlackBrainPath);
				}
			}
		});
		
		
		//Score
		JPanel jpFoodScore = new JPanel();
		jpFoodScore.setPreferredSize(new Dimension((int) (sideWidth),(int)(height*.10)));
		
		JLabel jlFoodScore = new JLabel("Food levels:");
		jlFoodScore.setPreferredSize(new Dimension((int) (sideWidth*.9),(int)(height*.04)));
		
		jtfRedScore = new JTextField();
		jtfRedScore.setText("Red: 0");
		jtfRedScore.setPreferredSize(new Dimension((int) (sideWidth*.45),(int)(height*.04)));
		jtfBlackScore = new JTextField();
		jtfBlackScore.setText("Black: 0");
		jtfBlackScore.setPreferredSize(new Dimension((int) (sideWidth*.45),(int)(height*.04)));
		
		jpFoodScore.add(jlFoodScore);
		jpFoodScore.add(jtfRedScore);
		jpFoodScore.add(jtfBlackScore);
		
		//Activity field
//		jtfActivity = new JTextField();
//		jtfActivity.setText("Activity:");
//		jtfActivity.setPreferredSize(new Dimension((int) (sideWidth*.8),(int)(height*.2)));
//		jpScore.add(jtfActivity);
		leftPanel.add(jpFoodScore);
		
		//Score
		JPanel jpNumAnts = new JPanel();
		jpNumAnts.setPreferredSize(new Dimension((int) (sideWidth),(int)(height*.10)));
		
		JLabel jlNumAnts = new JLabel("Num Ants:");
		jlNumAnts.setPreferredSize(new Dimension((int) (sideWidth*.9),(int)(height*.04)));
		
		jtfRedNumAnts = new JTextField();
		jtfRedNumAnts.setText("Red: 0");
		jtfRedNumAnts.setPreferredSize(new Dimension((int) (sideWidth*.45),(int)(height*.04)));
		jtfBlackNumAnts = new JTextField();
		jtfBlackNumAnts.setText("Black: 0");
		jtfBlackNumAnts.setPreferredSize(new Dimension((int) (sideWidth*.45),(int)(height*.04)));
		
		jpNumAnts.add(jlNumAnts);
		jpNumAnts.add(jtfRedNumAnts);
		jpNumAnts.add(jtfBlackNumAnts);
		
		leftPanel.add(jpNumAnts);
		
		//slider Stop and Reset Panel
		JPanel jpBottomPanel = new JPanel();
		jpBottomPanel.setPreferredSize(new Dimension((int) (sideWidth),(int)(height*.05)));
		JButton jbStart = new JButton();
		jbStart.setText("START");
		jbStart.setPreferredSize(new Dimension((int) (sideWidth*.4),(int)(height*.035)));
		jpBottomPanel.add(jbStart);
		
		//start
		jbStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(antGameViewport);
				createGUI(antGameViewport);
			}
		});
		
		
		// reset button
		JButton jbReset = new JButton();
		jbReset.setText("RESET");
		jbReset.setPreferredSize(new Dimension((int) (sideWidth*.25),(int)(height*.035)));
		jpBottomPanel.add(jbReset);
		leftPanel.add(jpBottomPanel);

		jbReset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				resetGUI();
			}
		});
		
		return leftPanel;
	}
	
	private void createGUI(Container panel){
		if(this.antGameGui == null){
			if(currentWorldPath == null){
				JOptionPane.showMessageDialog(panel, "No world file selected!");
			}else if(currentRedBrainPath == null){
				JOptionPane.showMessageDialog(panel, "No red brain file selected!");
			}else if(currentBlackBrainPath == null){
				JOptionPane.showMessageDialog(panel, "No black brain file selected!");
			}else{
				// only if filenames exist can the ant game be created
				this.antGameGui = new AntGameGUI(currentWorldPath,currentRedBrainPath,currentBlackBrainPath);
				
				if(this.antGameGui.brainAndWorldAreCorrect()){
					panel.add(this.antGameGui);
					this.antGameGui.init();
				}else{
					JOptionPane.showMessageDialog(panel, "Error processing the Ant brain or world occurred!");
					resetGUI();
				}
			}
		}
	}
	
	private void resetGUI(){
		
		if (antGameGui!=null){
			this.antGameViewport.remove(antGameGui);
			antGameGui.dispose();
		}
		antGameGui=null;
	}
	
	private void setCurrentWorldPath(String path){
		this.currentWorldPath = path;
	}

	public void setCurrentRedBrainPath(String currentRedBrainPath) {
		this.currentRedBrainPath = currentRedBrainPath;
	}

	public void setCurrentBlackBrainPath(String currentBlackBrainPath) {
		this.currentBlackBrainPath = currentBlackBrainPath;
	}
}
