import processing.core.*;

public class Hexagon {
	private PApplet app;
	private int xPos;
	private int yPos;
	private int sideLength;
	
	private Point[] points;
	
	private int color;
	
	public Hexagon(PApplet app, int xPos, int yPos, int sideLength){
		this.app = app;
		this.xPos = xPos;
		this.yPos = yPos;
		this.sideLength = sideLength;
		this.points = new Point[6];
		setPoints();
	}
	
	private void setPoints(){
		for (int i = 0; i < points.length; i++) {
			float r = PApplet.radians(i*60);
//			System.out.println(r+", "+sideLength+"   "+xPos+", "+yPos);
			int x = (int)(xPos + sideLength * PApplet.sin(r));
			int y = (int)(yPos + sideLength * PApplet.cos(r));
//			System.out.println(x+","+y);
			points[i] = new Point(x,y);
		}
	}
	
	public void draw(int nextColor){
		this.setColor(nextColor);
		this.app.fill(this.color);
		this.draw();
	}

	
	public void drawNoFill(){
		this.app.noFill();
		this.draw();
	}
	
	public void draw(){
//		this.app.noStroke();
//		stroke(0);
		
		this.app.beginShape();
		for (Point p : this.getPoints()) {
			this.app.vertex(p.getX(),p.getY());
		}
		Point firstPoint = this.getPoints()[0];
		this.app.vertex(firstPoint.getX(), firstPoint.getY());
		this.app.endShape();
	}
	
	public void drawMarkers(boolean[] redMarkers, boolean[] blackMarkers){
		// red markers
		this.app.stroke(255,0,0);
		
		if(redMarkers[0]){
			this.app.point(xPos-4,yPos-4);
			this.app.point(xPos-3,yPos-4);
		}
		if(redMarkers[1]){
			this.app.point(xPos-1,yPos-4);
			this.app.point(xPos  ,yPos-4);
		}
		if(redMarkers[2]){
			this.app.point(xPos+2,yPos-4);
			this.app.point(xPos+3,yPos-4);
		}
		if(redMarkers[3]){
			this.app.point(xPos-4,yPos-2);
			this.app.point(xPos-3,yPos-2);
		}
		if(redMarkers[4]){
			this.app.point(xPos-1,yPos-2);
			this.app.point(xPos  ,yPos-2);
		}
		if(redMarkers[5]){
			this.app.point(xPos+2,yPos-2);
			this.app.point(xPos+3,yPos-2);
		}
		
		// black markers
		this.app.stroke(0,0,0);

		if(blackMarkers[0]){
			this.app.point(xPos-4,yPos+1);
			this.app.point(xPos-3,yPos+1);
		}
		if(blackMarkers[1]){
			this.app.point(xPos-1,yPos+1);
			this.app.point(xPos  ,yPos+1);
		}
		if(blackMarkers[2]){
			this.app.point(xPos+2,yPos+1);
			this.app.point(xPos+3,yPos+1);
		}
		if(blackMarkers[3]){
			this.app.point(xPos-4,yPos+3);
			this.app.point(xPos-3,yPos+3);
		}
		if(blackMarkers[4]){
			this.app.point(xPos-1,yPos+3);
			this.app.point(xPos  ,yPos+3);
		}
		if(blackMarkers[5]){
			this.app.point(xPos+2,yPos+3);
			this.app.point(xPos+3,yPos+3);
		}
	}
	
	public Point[] getPoints(){
		return this.points;
	}

	public int getxPos() {
		return xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public int getSideLength() {
		return sideLength;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	
	
}
