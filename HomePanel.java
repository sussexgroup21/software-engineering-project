import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class HomePanel extends JFrame{
	private int width;
	private int height;
	
	public HomePanel(int width, int height){
		this.width = width;
		this.height = height;
		
		this.setPreferredSize(new Dimension((int)(width*.6),(int)(height*.35)));
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.add(mainPanel());
		
		this.pack();
		
		this.setVisible(true);
	}
	
	private JPanel mainPanel(){
		final JPanel jpMainPanel = new JPanel();
		
		jpMainPanel.setBackground(Color.GREEN);
		Dimension dButton = new Dimension((int)(this.width*.3),(int)(this.height*.07));
		//set up the layout
		// two player option button
		JButton jb1 = mainPanelButton("Two Play", dButton);
		
		//onClickListeners
		jb1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				HomePanel.this.dispose();
				GamePanel gp = new GamePanel(width, height);
			}
		});
		
		jpMainPanel.add(jb1);

		// tournament option button
		JButton jb2 = mainPanelButton("Tournament", dButton);
		
		//tournament GUI
		jb2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				TournamentPanel tp = new TournamentPanel(width, height);
				HomePanel.this.dispose();
			}
		});
		
		jpMainPanel.add(jb2);
		
		// help option button
		JButton jb4 = mainPanelButton("Help", dButton);
		
		jb4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				help(jpMainPanel,HomePanel.this);
			}
		});
		
		jpMainPanel.add(jb4);
		
		return jpMainPanel;
	}
	
	private JButton mainPanelButton(String caption, Dimension dButton){
		JButton btn = new JButton();
		btn.setAlignmentX(Component.CENTER_ALIGNMENT);
		btn.setAlignmentY(Component.CENTER_ALIGNMENT);
		btn.setText(caption);
		btn.setPreferredSize(dButton);
		return btn;
	}
	
	/**
	 * panel is removed from JFrame 
	 * new panel created and added with help data
	 * help panel removed and original added
	 * 
	 * @param panel
	 * @param jfPanel
	 */
	public void help(final Container panel,final JFrame jfPanel){
		int x = panel.getWidth();
		int y = panel.getHeight();
		jfPanel.remove(panel); //remove Original panel
		//set help text here!!!
		final JPanel jpHelpPanel = new JPanel(); //create help pannel
		JTextField tfHelp = new JTextField();
		tfHelp.setText("Click the buttons");
		tfHelp.setEditable(false);
		tfHelp.setPreferredSize(new Dimension((int) (x*.8),(int)(y*.7)));
		jpHelpPanel.add(tfHelp);
		JButton jbBack = new JButton();
		jbBack.setText("home");
		jbBack.setPreferredSize(new Dimension((int) (x*.4),(int)(y*.2)));
		jpHelpPanel.add(jbBack);
		jpHelpPanel.validate();
		jpHelpPanel.repaint();
		jfPanel.add(jpHelpPanel); //add help panel
		jfPanel.validate(); //refreshes
		jfPanel.repaint();
		
		//go home
		jbBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				jfPanel.remove(jpHelpPanel); //remove help panel and add original
				jfPanel.add(panel);
				jfPanel.validate(); //refreshes
				jfPanel.repaint();
			}
		});
		
	}
	
	public static void main(String[] args){
		HomePanel hp = new HomePanel(1000, 1000);
	}
}
