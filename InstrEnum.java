
public enum InstrEnum {
	Drop,
	Flip,
	Mark,
	Move,
	PickUp,
	Sense,
	Turn,
	Unmark;
}
