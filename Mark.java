public class Mark extends Instruction {
	int m;
	int s;
	
	public Mark(String i, String marker, String state){
		super(i);
		this.m = Integer.parseInt(marker);
		this.s = Integer.parseInt(state);
	}
	
	public int nextState(){
		return this.s;
	}
	
	public int marker(){
		return this.m;
	}
	
	
}