public class Move extends Instruction {
	int failState;
	int successState;
	
	public Move(String i, String st1, String st2){
		super(i);
		this.failState = Integer.parseInt(st2);
		this.successState = Integer.parseInt(st1);
	}
	
	public int nextState(){
		return this.successState;
	}
	public int failState(){
		return this.failState;
	}
}