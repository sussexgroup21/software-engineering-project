public class PickUp extends Instruction {
	int successState;
	int failState;
	
	public PickUp(String i, String st1, String st2){
		super(i);
		this.successState = Integer.parseInt(st1);
		this.failState = Integer.parseInt(st2);
	}
	
	public int nextState(){
		return this.successState;
	}
	public int failState(){
		return this.failState;
	}	
	
}