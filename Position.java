public class Position {
	private int row;
	private int col;

	public Position(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	@Override
	public String toString() {
		return "(row:" + row + ", col:" + col + ")";
	}

	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}
}
