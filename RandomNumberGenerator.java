import java.math.BigInteger;
/**
 * Random number generator, using BigInteger.
 * 
 * Firstly it calculates a sequence of numbers s0 ... si, using the relation:
 * s0 = some seed number
 * s[i+1] = (s[i] * 22695477 + 1) % 2^32 
 * 
 * This is a Linear Congruential Generator, like the one used in Borland C/C++ (http://en.wikipedia.org/wiki/Linear_congruential_generator)
 * 
 * From that the ith output value x[i] is calculated:
 * x[i] = (s[i+4] / 65536) % mod 16384
 * 
 * (division is integer division)
 * 
 * Since Flip takes in a number n as a parameter (e.g. n = 2 means that random numbers 0, 1 and 2 can be generated) the output is:
 * x[i] mod n
 * 
 * @author rjw36
 *
 */
public class RandomNumberGenerator {
	private BigInteger lastSeed;
	private BigInteger a = BigInteger.valueOf(22695477);
	private BigInteger B_16384 = BigInteger.valueOf(16384);
	private BigInteger B_2_32 = BigInteger.valueOf(2).pow(32);
	
	public RandomNumberGenerator(int s0){
		// because we want to be using si+4, then generate s1, s2, s3 and s4
		BigInteger s1 = getNextSeed(BigInteger.valueOf(s0));
		BigInteger s2 = getNextSeed(s1);
		BigInteger s3 = getNextSeed(s2);
		lastSeed = getNextSeed(s3);
	}
	
	public int randomint(int n){
		// get the last seed
		BigInteger currentSeed = lastSeed;
		// generate the seed for the next call of randomint
		lastSeed = getNextSeed(currentSeed);
		
		BigInteger x = getX(currentSeed);
		BigInteger p = x.mod(BigInteger.valueOf(n));
		/*
		System.out.println("---");
		System.out.println(lastSeed);
		System.out.println(x);
		System.out.println(p);
		System.out.println("---");
		*/
		return p.intValue();
	}	
	
	private BigInteger getNextSeed(BigInteger s1){
		return (s1.multiply(a)).add(BigInteger.ONE).mod(B_2_32);
	}
	
	private BigInteger getX(BigInteger s){
		return (s.shiftRight(16)).mod(B_16384);
	}
	
	public static void main(String[] args){
		int s0 = 12345;
		RandomNumberGenerator rng = new RandomNumberGenerator(s0);
		
		for (int i = 0; i < 20000; i++) {
			System.out.println(rng.randomint(16383));
		}
	}
}

