public class Sense extends Instruction {
	String sensedir;
	int failState;
	int successState;
	String condition;
	
	public Sense(String i, String sensedir, String st1, String st2, String cond){
		super(i);
		this.failState = Integer.parseInt(st2);
		this.successState = Integer.parseInt(st1);
		this.sensedir = sensedir;
		this.condition = cond;
	}
	
	public int nextState(){
		return this.successState;
	}
	public int failState(){
		return this.failState;
	}	
	public String condition(){
		return this.condition;
	}
	public String senseDir(){
		return this.sensedir;
	}
	
}