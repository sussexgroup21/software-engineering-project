
public enum SenseDirEnum {
	Here,
	Ahead,
	LeftAhead,
	RightAhead;
}
