import java.util.ArrayList;

import javax.swing.JTextField;

/**
 * This class provides the framework for hosting a tournament
 * @author mw277
 *
 */
public class Tournament implements Runnable {
	int rounds;
	String antWorld;
	ArrayList<BrainPanel> antBrains = new ArrayList<BrainPanel>();
	JTextField jtfRunStatus;
	public Tournament(ArrayList<BrainPanel> antBrains,int rounds,String antWorld,JTextField jtfRunStatus) {
		this.jtfRunStatus = jtfRunStatus;
		this.antWorld = antWorld;
		this.antBrains = antBrains;
		this.rounds = rounds;
	}
	public void playTounament(){
		System.out.println("Tournament Starting"+"Rounds:"+rounds);
		jtfRunStatus.setText("RUNNING");
		for (int i=0;i<rounds;i++){
			//BrainPanel redBrain:antBrains
			for (BrainPanel redBrain:antBrains){
				for (BrainPanel blackBrain:antBrains){
					if (redBrain!=blackBrain){
						AntWorld aw = new AntWorld(antWorld,redBrain.brainName,blackBrain.brainName);
						//while (true){
						//who ever has most food at end wins
						aw.playRounds(100000);	
						if (aw.blackFood>aw.redFood){
							blackBrain.score++;
							blackBrain.jbScore.setText(blackBrain.score+"");
							System.out.println("InBLACK");
						} else {
							redBrain.score++;
							System.out.println("inRED");
							redBrain.jbScore.setText(redBrain.score+"");
						}
					}
				}
			}
		}
		for (BrainPanel bp:antBrains){
			System.out.println("Score"+bp.score);
		}
		jtfRunStatus.setText("FINISHED");
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		playTounament();
	}
}
	