import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class TournamentPanel extends JFrame {
	JTextField jtfPercent;
	private int width;
	private int height;
	private int sideWidth;
	
	private ArrayList<BrainPanel> alTournamentBrains;
	private JPanel jpDisplayBrains;
	private JTextField jtfWorld;
	private Thread t;
	private Tournament tourn;
	public TournamentPanel(int width, int height){
		this.width = width;
		this.height = height;
		this.sideWidth = (int) (width*.3);
		this.alTournamentBrains = new ArrayList<BrainPanel>();
		
		JPanel jpMainPanel = new JPanel();
		
		BoxLayout b = new BoxLayout(jpMainPanel,BoxLayout.X_AXIS);
		jpMainPanel.setLayout(b);
		
		jpMainPanel.add(leftPanel());
		jpMainPanel.add(rightPanel());
		
		this.add(jpMainPanel);
		
		this.setResizable(false);
		this.setVisible(true);
		this.pack();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private JPanel leftPanel(){
		final JPanel leftPanel = new JPanel();
		leftPanel.setPreferredSize(new Dimension((int) (sideWidth*.75),(int)(height*.75)));
		//leftPanel.setBackground(Color.RED);

		//left panel additions
		JButton jbHome = new JButton();
		jbHome.setText("Home");
		jbHome.setPreferredSize(new Dimension((int) (sideWidth*.75),(int)(height*.1)));
		leftPanel.add(jbHome);
		
		jbHome.addActionListener(new ActionListener() {
			
			@Override	
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				HomePanel hp = new HomePanel(width, height);
				TournamentPanel.this.dispose();
				if (t!=null){
					t.stop();
					t=null; //tidy thread
				}
			}
		});
		
		//Select World
		JPanel jpSelectWorld = new JPanel();
		jpSelectWorld.setPreferredSize(new Dimension((int) (sideWidth*.75),(int)(height*.13)));
		jtfWorld = new JTextField();
		jtfWorld.setText("World Name");
		jtfWorld.setPreferredSize(new Dimension((int) (sideWidth*.75),(int)(height*.0625)));
		jpSelectWorld.add(jtfWorld);
		JButton jbBrowseWorld = new JButton();
		jbBrowseWorld.setText("Browse"); 
		jbBrowseWorld.setPreferredSize(new Dimension((int) (sideWidth*.4),(int)(height*.04)));
		jpSelectWorld.add(jbBrowseWorld);
		JButton jbRandomWorld = new JButton();
		jbRandomWorld.setText("?");
		jbRandomWorld.setPreferredSize(new Dimension((int) (sideWidth*.2),(int)(height*.04)));
		jpSelectWorld.add(jbRandomWorld);
		leftPanel.add(jpSelectWorld);
		
		//Brains TextFeild
		JTextField jtfBrains = new JTextField();
		jtfBrains.setPreferredSize(new Dimension((int) (sideWidth*.75),(int)(height*.05)));
		jtfBrains.setText("Brains");
		jtfBrains.setEditable(false);
		jtfBrains.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftPanel.add(jtfBrains);
		
		//Add Brain Button 
		JButton jbAddBrain = new JButton();
		jbAddBrain.setText("Add \nBrain");
		jbAddBrain.setPreferredSize(new Dimension((int) (sideWidth*.75),(int)(height*.07)));
		leftPanel.add(jbAddBrain);
		//RUN ALL
		JButton jbRunAll = new JButton();
		jbRunAll.setText("Run All");
		jbRunAll.setPreferredSize(new Dimension((int) (sideWidth*.4),(int)(height*.07)));
		leftPanel.add(jbRunAll);
		// Rounds remaining text field
		final JTextField jtfRoundsRemaing = new JTextField();
		jtfRoundsRemaing.setText("1");
		jtfRoundsRemaing.setPreferredSize(new Dimension((int) (sideWidth*.2),(int)(height*.03)));
		leftPanel.add(jtfRoundsRemaing);
		//OnClisteners
		//browse world
		
		final JFileChooser jfc = new JFileChooser("./worlds");
		jbBrowseWorld.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = jfc.showOpenDialog(leftPanel);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					//BrainPanel.this.brainName = jfc.getSelectedFile().getPath();
					//jbName.setText(BrainPanel.this.brainName);
					jtfWorld.setText(jfc.getSelectedFile().getPath());
				}
				
			}
		});
		jbRunAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int noRounds = (int)Integer.parseInt(jtfRoundsRemaing.getText());
				String worldLocation = jtfWorld.getText();
				tourn = new Tournament(alTournamentBrains,noRounds,worldLocation,jtfPercent); 
				if (t!=null){
					t.stop();
					t=null;
					//System.gc();
				}
				t = new Thread(tourn);
				t.start();
				
			}
		});
		jbAddBrain.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BrainPanel brainPanel = new BrainPanel("File", jpDisplayBrains, TournamentPanel.this, alTournamentBrains, width, height);
				brainPanel.setPreferredSize(new Dimension((int) (width*.6),(int)(height*.075)));
				brainPanel.setMaximumSize(new Dimension((int) (width*.6),(int)(height*.075)));
				jpDisplayBrains.add(brainPanel);
				alTournamentBrains.add(brainPanel);
				TournamentPanel.this.setVisible(true);
				TournamentPanel.this.pack();
			}
		});
		return leftPanel;
	}
	
	private JPanel rightPanel(){
		JPanel rightPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension((int) (width*.75),(int)(height*.75))); //contains processing applet		
		//rightPanel.setBackground(Color.YELLOW);
		
		// Right Panel
		JTextField jtfScore = new JTextField();
		jtfScore.setPreferredSize(new Dimension((int) (width*.5),(int)(height*.1)));
		jtfScore.setText("Tournament Score");
		jtfScore.setEditable(false);
		jtfScore.setAlignmentX(Component.CENTER_ALIGNMENT);
		rightPanel.add(jtfScore);
		
		jtfPercent = new JTextField();
		jtfPercent.setPreferredSize(new Dimension((int) (width*.2),(int)(height*.1)));
		jtfPercent.setText("WAITING");
		Font f = new Font("Arial",Font.BOLD,30);
		jtfPercent.setFont(f);
		jtfPercent.setEditable(false);
		jtfPercent.setAlignmentX(Component.CENTER_ALIGNMENT);
		rightPanel.add(jtfPercent);
		
		// Display Brains
		this.jpDisplayBrains = new JPanel();
		BoxLayout b4 = new BoxLayout(jpDisplayBrains,BoxLayout.Y_AXIS);
		jpDisplayBrains.setLayout(b4);
		JScrollPane jspDisplayBrains = new JScrollPane(jpDisplayBrains);
		jpDisplayBrains.setAutoscrolls(true);
		jspDisplayBrains.setPreferredSize(new Dimension((int) (width*.7),(int)(height*.63)));
		rightPanel.add(jspDisplayBrains);
		
		return rightPanel;
	}
}
