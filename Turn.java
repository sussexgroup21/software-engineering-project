public class Turn extends Instruction {
	int s;
	String lr;
	
	public Turn(String i, String lr, String state){
		super(i);
		this.s = Integer.parseInt(state);
		this.lr = lr;
	}
	
	public int nextState(){
		return this.s;
	}

	public String getDirection(){
		return this.lr;
	}
}